using LinearAlgebra

function multiplica(a, b)
	dima = size(a)
	dimb = size(b)
	if dima[2] != dimb[1]
		return -1
	end
	c = zeros(dima[1], dimb[2])
	for i in 1:dima[1]
		for j in 1:dimb[2]
			for k in 1:dima[2]
				c[i, j] = c[i, j] + a[i, k] * b[k, j]
			end
		end
	end
	return c
end

function matrix_pot(M, p)
	x = M
	for i in 2:p
		x = multiplica(x, M)
	end
	return x
end

function matrix_pot_by_squaring(M, p)
	if p == 0	
		return 1
	elseif p == 1
		return M
	elseif p % 2 == 0
		y = multiplica(M, M)
		x = matrix_pot_by_squaring(y, p / 2)
		return x
	else
		y = multiplica(M, M)
		x = matrix_pot_by_squaring(y, (p - 1) / 2)
		return multiplica(M, x)
	end
end

function testes()
	if matrix_pot([1 2 ; 3 4], 1) == matrix_pot_by_squaring([1 2 ; 3 4], 1)
		if matrix_pot([1 2 ; 3 4], 2) == matrix_pot_by_squaring([1 2 ; 3 4], 2)
			if matrix_pot([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7) == matrix_pot_by_squaring([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7)
				println("Passou nos testes!")
				println("Respostas: ")
				println(matrix_pot([1 2 ; 3 4], 1))
				println(matrix_pot([1 2 ; 3 4], 2))
				println(matrix_pot([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7)) 
			else
				println("Teste 3 falhou!")
			end
		else
			println("Teste 2 falhou!")
		end
	else	
		println("Teste 1 falhou!")
	end
end

function compare_times()
	M = Matrix(LinearAlgebra.I, 30, 30)
	@time matrix_pot(M, 10)
	@time matrix_pot_by_squaring(M, 10)
end

testes()
compare_times()

